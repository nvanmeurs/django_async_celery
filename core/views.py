from django.http import HttpResponse
from django.shortcuts import render
from celery import group
from .tasks import add

def debug(request):
    results = group(add.s(i, i) for i in xrange(1000))().get()
    return HttpResponse(results)